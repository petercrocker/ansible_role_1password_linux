# Ansible Role: 1password on Ubuntu 20.04

A quick and dirty ansible role to add [1password linux](https://1password.com/downloads/linux/) to a host running Ubuntu 20.04.

## Requirements

- Tested on Ubuntu 20.04.

## Role Variables

None

## Usage

```yaml
- hosts: myserver
  become: true
  roles:
    - { role: 1password_linux, tags: ["1password_linux"] }
```

## License

[Unlicense](https://unlicense.org)

## Author Information

This role was created in 2021 by [Pete Crocker](http://petecrocker.com/).
